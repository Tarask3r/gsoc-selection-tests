var TestSelectionModule = (function (module) {

	var tableHeaders = [],
		defaultUrl = "./data.tsv",
		data = {},
		statistic = {},
		def = $.Deferred(),
		ascending = false,
		statisticHeader= [];

	module.init = function(url){
		defaultUrl = url || defaultUrl;
		d3.tsv(defaultUrl, function(error, d){
			if(error) throw error;
			var species, width = {};
			data = d;
			tableHeaders = d3.keys(data[0]);
			data.forEach(function(d){
				species = d.species;
				width = d.sepalWidth;
				statistic[species] = statistic[species] || [{"label" : "<= 2.5", "value" : 0},
																										{"label" : ">2.5 & <=3.0", "value" : 0},
																										{"label" : ">3.0 & <=3.5", "value" : 0},
																										{"label" : ">3.5 & <=4", "value" : 0},
																										{"label" : "> 4", "value" : 0}]
				if(width <= 2.5){
					statistic[species][0].value++;
				}
				else if (2.5 < width && width <= 3) {
					statistic[species][1].value++;
				}
				else if (3 < width && width <= 3.5) {
					statistic[species][2].value++;
				}
				else if (3.5 < width && width <= 4) {
					statistic[species][3].value++;
				}
				else{
					statistic[species][4].value++;
				}
			});
			statisticHeader = d3.keys(statistic);
			def.resolve();
		});
		return module;
	};

	module.renderTable = function(d3Target){
		def.done(function(){
			render(d3Target);
		});
		function render(d3Target){
			var table = d3Target.append("table"),
			thead = table.append("thead"),
			tbody = table.append("tbody"),
			rows, cells;
			thead.append("tr")
				.selectAll("th")
				.data(tableHeaders).enter()
				.append("th")
				.text(function(header){ return header; })
				.on('click', function (target) {
					if(ascending){
						ascending = false;
						rows.sort(function (a, b) {
							return d3.descending(a[target], b[target]);
						});
					}
					else{
						ascending = true;
						rows.sort(function (a, b) {
							return d3.ascending(a[target], b[target]);
						});
					}
				});

			rows = tbody.selectAll("tr")
				.data(data).enter()
				.append("tr");

			cells = rows.selectAll("td")
				.data(function(row) { return tableHeaders.map(function(tableHeaders) { return {column: tableHeaders, value: row[tableHeaders]}; }); })
				.enter()
				.append("td")
				.html(function(d) { return d.value; });

		}
		return module;
	};

	module.renderChart = function(d3Target){
		var width = 600,
				height = 400,
				legendRectSize = 18,
        legendSpacing = 4,
				svg, g, color, min,
				oRadius, iRadius, pie,
				arc, path, el,  data, legend, header, label;

		def.done(function(){
			svg = d3Target.append("svg")
				.attr("height", height)
				.attr("width", width);
			g = svg.append('g')
				.attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
			color = d3.scale.category10();
			min = Math.min(width, height);
			oRadius = min / 2 * 0.9;
			iRadius = min / 2 * 0.85;
			pie = d3.layout.pie().value(function(d){ return d.value; }).sort(null);
			arc = d3.svg.arc()
				.outerRadius(oRadius)
				.innerRadius(iRadius);
			el = statisticHeader.shift();
			statisticHeader.push(el);

    	label = svg.append('text').text(el)
	      .attr('x', 10)
	      .attr('y', 20)
	      .attr('fill', 'black')
				.attr("class", "label");


			data = statistic[el];
			path = g.datum(data).selectAll("path")
				.data(pie)
				.enter().append("path")
					.attr("class","piechart")
					.attr("fill", function(d,i){ return color(d.data.label); })
					.attr("d", arc)
				.each(function(d){
					this._current = d; });
			legend = g.selectAll('.legend')
        .data(color.domain())
        .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', function(d, i) {
          var height = legendRectSize + legendSpacing;
          var offset =  height * color.domain().length / 2;
          var horz = -2 * legendRectSize;
          var vert = i * height - offset;
          return 'translate(' + horz + ',' + vert + ')';
        });

      legend.append('rect')
        	.attr('width', legendRectSize)
        	.attr('height', legendRectSize)
        .style('fill', color)
        .style('stroke', color);

      legend.append('text')
        	.attr('x', legendRectSize + legendSpacing)
        	.attr('y', legendRectSize - legendSpacing)
        .text(function(d) { return d; });

			setInterval(render,5000);
		});

		function render(){
			el = statisticHeader.shift();
			statisticHeader.push(el);
			label.text(el);
			data = statistic[el];
			g.datum(data).selectAll("path").data(pie).transition().duration(1000).attrTween("d", arcTween)
			g.datum(data).selectAll("path")
				.data(pie)
			.enter().append("path")
				.attr("class","piechart")
				.attr("fill", function(d,i){ return color(i); })
				.attr("d", arc)
				.each(function(d){ this._current = d; })
			g.datum(data).selectAll("path")
				.data(pie).exit().remove();

		}
		function arcTween(a) {
		  var i = d3.interpolate(this._current, a);
		  this._current = i(0);
		  return function(t) {
		  	return arc(i(t));
		  };
		}

		return module;
	};

	return module;

}(TestSelectionModule || {}));

TestSelectionModule.init();
TestSelectionModule.renderTable(d3.select("#container"));
TestSelectionModule.renderChart(d3.select("#container"));
